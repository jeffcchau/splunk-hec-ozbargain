from splunkHEC import SplunkHEC
from scrape import Scrape
import yaml

with open("config.yml", 'r') as stream:
    try:
        config = yaml.safe_load(stream)['config']
    except yaml.YAMLError as exc:
        print(exc)

hec = SplunkHEC(config['token'], config['splunk_host'], config['splunk_port'])

metadata = {"sourcetype": "test"}

# ozbargain scrape
url = "http://www.ozbargain.com.au"
scrape = Scrape(url)

deals = scrape.soup.find_all("div", "node-ozbdeal")
nodes = []

for index, deal in enumerate(deals):
    nodes.append(deal.find("h2", "title").text)

splunk_event = {"nodes":nodes}
print hec.send(splunk_event, metadata)
