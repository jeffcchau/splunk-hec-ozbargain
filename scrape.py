import urllib2
from bs4 import BeautifulSoup

class Scrape:
    def __init__(self, url):
            if not 'http' in url:
                raise("no http or https found in url")
            self.url = url
            self.page = urllib2.urlopen(url)
            self.soup = BeautifulSoup(self.page, "html.parser")
