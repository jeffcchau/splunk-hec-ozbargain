import requests
import json

class SplunkHEC:
    def __init__(self, token, splunk_host, port='8088'):
        if not 'http' in splunk_host:
            raise("no http or https found in hostname")
        self.token=token
        self.url=splunk_host+":"+port+"/services/collector/event"
        self.port=port

    def send(self, event, metadata=None):
        headers = {"Authorization": "Splunk "+self.token}
        payload = {"host": self.url, "event": event}

        if metadata:
            payload.update(metadata)

        r = requests.post(self.url, data=json.dumps(payload), headers=headers, verify=True if 'https' in self.url else False)
        return r.status_code, r.text,
