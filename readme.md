# Splunk HEC Example
## BeautifulSoup + SplunkHEC

See config.yml.example and input your Splunk instance details. Set up a HEC token in the Splunk interface.

Run the example with
`python ozbargain.py`

Create your own page scraper with this sample template and the BeautifulSoup docs.
